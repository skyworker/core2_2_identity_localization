﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace core2_2_identity_localization.Services
{
    public interface ILocalizedValidationAttributeAdapter
    {
        string GetErrorMessageResourceName(ValidationAttribute attribute);

        bool CanHandle(ValidationAttribute attribute);
    }
}
