﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace core2_2_identity_localization.Services
{
    public interface ILocalizedValidationMetadataProvider : IValidationMetadataProvider
    {
        IList<ILocalizedValidationAttributeAdapter> Adapters { get; }
    }
}
